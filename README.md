---
license: mit
tags:
- financial
- ner
- context-ner
size_categories:
- 1M<n<10M
---

# EDGAR10-Q

## Dataset Summary

EDGAR10-Q is a large financial dataset curated by scraping annual and quarterly reports of top 1500 LLCs in the world. 
The dataset is designed for the task of ContextNER, which aims to generate the relevant context for entities in a sentence, where the context is a set of phrases describing the entity but not necessarily present in the sentence. 
The dataset is the largest in terms of the number of sentences (1M), entities (2.8M), and average tokens per sentence (35).

You may want to check out 
* Our paper: [CONTEXT-NER: Contextual Phrase Generation at Scale](https://arxiv.org/abs/2109.08079/)
* GitHub: [Click Here](https://github.com/him1411/edgar10q-dataset)



## Supported Tasks

The dataset is designed for the task of ContextNER that aims to generate the relevant context for entities in a sentence, 
where the context is a set of phrases describing the entity but not necessarily present in the sentence. 

## Dataset Structure

### Data Instances

The dataset includes plain text input-output pairs, where the input is a sentence with an entity and the output is the context for the entity.

An example of a train instance looks as follows:


```
{
"input": "0.6 million . The denominator also includes the dilutive effect of approximately 0.9 million, 0.6 million and 0.6 million shares of unvested restricted shares of common stock for the years ended December 31, 2019, 2018 and 2017, respectively.",
"output": "Dilutive effect of unvested restricted shares of Class A common stock"
}
```


We also publish a metadata file in the original repository to promote future research in the area. Please checkout the [main website](https://github.com/him1411/edgar10q-dataset)


### Data Fields

The data fields are the same among all splits.

- `text`: a `string` in the form of entity plus sentence. 
- `label`: a string describing the relevant context for entity in the sentence

### Data Splits

The dataset is split into train, validation, and test sets. The sizes of the splits are as follows:

|           | Train     | Validation | Test  |
|-----------|-----------|------------|-------|
| Instances | 1,498,995 | 187,383    |187,383|

### Dataset Creation

The dataset was created by scraping annual and quarterly reports of top 1500 LLCs in the world.


### Models trained using this dataset

There are several models finetuned using this dataset. They are: 

1. [EDGAR-T5-base](https://huggingface.co/him1411/EDGAR-T5-base)
2. [EDGAR-BART-Base](https://huggingface.co/him1411/EDGAR-BART-Base)
3. [EDGAR-flan-t5-base](https://huggingface.co/him1411/EDGAR-flan-t5-base)
4. [EDGAR-T5-Large](https://huggingface.co/him1411/EDGAR-T5-Large)
5. [EDGAR-Tk-Instruct-Large](https://huggingface.co/him1411/EDGAR-Tk-Instruct-Large)
6. [Instruction tuned EDGAR-Tk-Instruct-base](https://huggingface.co/him1411/EDGAR-Tk-instruct-base-inst-tune)

### Citation Information

If you use this dataset and any other related artifact, please cite the following paper:

```
@article{gupta2021context,
  title={Context-NER: Contextual Phrase Generation at Scale},
  author={Gupta, Himanshu and Verma, Shreyas and Kumar, Tarun and Mishra, Swaroop and Agrawal, Tamanna and Badugu, Amogh and Bhatt, Himanshu Sharad},
  journal={arXiv preprint arXiv:2109.08079},
  year={2021}
}
```

### Contributions

Thanks to [@him1411](https://github.com/him1411) for adding this dataset.